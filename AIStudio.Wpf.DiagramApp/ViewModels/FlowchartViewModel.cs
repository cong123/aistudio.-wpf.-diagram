﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.DiagramApp.ViewModels
{
    public class FlowchartViewModel : PageViewModel
    {
        public FlowchartViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {
       
        }
        public FlowchartViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {          
            if (DiagramViewModel != null)
            {
                FlowchartService.InitData(DiagramViewModel.Items.OfType<FlowNode>().ToList(), DiagramViewModel.Items.OfType<ConnectionViewModel>().ToList(), DiagramViewModel, false);
            }
            _service.DrawModeViewModel.LineDrawMode = DrawMode.ConnectingLineSmooth;
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.DiagramOption.LayoutOption.ShowGrid = true;
            DiagramViewModel.DiagramOption.LayoutOption.GridCellSize = new Size(100, 100);
            DiagramViewModel.DiagramOption.LayoutOption.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.CellVerticalAlignment = CellVerticalAlignment.Center;
            _service.DrawModeViewModel.LineDrawMode = DrawMode.ConnectingLineSmooth;
        }

        protected override void Init(bool initNew)
        {
            base.Init(initNew);

            DesignerItemViewModelBase start = new StartFlowNode() { Left = 100, Top = 0, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString() };
            DiagramViewModel.Add(start);

            DesignerItemViewModelBase middle1 = new MiddleFlowNode() { Left = 100, Top = 100, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString(), Text = "主管审批（两人或）", UserIds= new List<string> { "操作员1", "操作员2" }, ActType = "or", SimulateApprove = true };
            DiagramViewModel.Add(middle1);

            DesignerItemViewModelBase decide = new DecideFlowNode() { Left = 100, Top = 200, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString(), Text = "5" };
            DiagramViewModel.Add(decide);

            DesignerItemViewModelBase middle2 = new MiddleFlowNode() { Left = 200, Top = 300, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString(), Text = "分管领导（两人与）", UserIds = new List<string> { "操作员1", "操作员2" }, ActType = "and", SimulateApprove = true };
            DiagramViewModel.Add(middle2);

            DesignerItemViewModelBase cobegin = new COBeginFlowNode() { Left = 100, Top = 400, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString() };
            DiagramViewModel.Add(cobegin);

            DesignerItemViewModelBase middle3 = new MiddleFlowNode() { Left = 100, Top = 500, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString(), Text = "财务审批", UserIds = new List<string> { "Admin" }, ActType = "or", SimulateApprove = true };
            DiagramViewModel.Add(middle3);

            DesignerItemViewModelBase middle4 = new MiddleFlowNode() { Left = 200, Top = 500, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString(), Text = "人力审批", RoleIds = new List<string> { "操作员", "管理员" }, ActType = "or", SimulateApprove = true };
            DiagramViewModel.Add(middle4);

            DesignerItemViewModelBase coend = new COEndFlowNode() { Left = 100, Top = 600, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString() };
            DiagramViewModel.Add(coend);

            DesignerItemViewModelBase end = new EndFlowNode() { Left = 100, Top = 700, ItemWidth = 80, ItemHeight = 40, StatusColor = Colors.Yellow.ToString() };
            DiagramViewModel.Add(end);

            ConnectionViewModel connector1 = new ConnectionViewModel(start.BottomConnector, middle1.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);     
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(middle1.BottomConnector, decide.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector2);

            ConnectionViewModel connector3 = new ConnectionViewModel(decide.RightConnector, middle2.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector3);
            connector3.AddLabel(">=3");

            ConnectionViewModel connector4 = new ConnectionViewModel(middle2.BottomConnector, cobegin.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector4);

            ConnectionViewModel connector5 = new ConnectionViewModel(decide.BottomConnector, cobegin.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector5);
            connector5.AddLabel("<3");

            ConnectionViewModel connector6 = new ConnectionViewModel(cobegin.BottomConnector, middle3.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector6);

            ConnectionViewModel connector7 = new ConnectionViewModel(cobegin.BottomConnector, middle4.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector7);

            ConnectionViewModel connector8 = new ConnectionViewModel(middle3.BottomConnector, coend.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector8);

            ConnectionViewModel connector9 = new ConnectionViewModel(middle4.BottomConnector, coend.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector9);

            ConnectionViewModel connector10 = new ConnectionViewModel(coend.BottomConnector, end.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.Add(connector10);

            TextDesignerItemViewModel despcription = new TextDesignerItemViewModel()
            {
                Name = nameof(despcription),
                Left = 360,
                Top = 60,
                ItemWidth = 300,
                ItemHeight = 150,
                Text = @"
      一个简易的OA审批流程
      1.主管审批为or审批，任意一个审批即可；
      2.分管领导为and审批，需要审批两次；
      3.财务审批为or审批，任意一个审批即可；
      4.人力审批为or审批，任意一个审批即可；
注意：为了模拟审批，双击节点可以进行审批；请双击主管审批、分管领导、财务审批、人力审批、进行效果查看。"
            };
            despcription.FontViewModel.HorizontalAlignment = HorizontalAlignment.Left;
            despcription.FontViewModel.VerticalAlignment = VerticalAlignment.Top;
            despcription.FontViewModel.FontColor = Colors.Blue;
            DiagramViewModel.Add(despcription);

            FlowchartService.InitData(DiagramViewModel.Items.OfType<FlowNode>().ToList(), DiagramViewModel.Items.OfType<ConnectionViewModel>().ToList(), DiagramViewModel, true);
        }
  

        public override void Dispose()
        {
            base.Dispose();

            foreach (var viewModel in DiagramViewModels)
            {
                FlowchartService.Dispose(viewModel);
            }
        }
    }
}
