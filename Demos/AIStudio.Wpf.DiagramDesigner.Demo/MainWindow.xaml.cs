﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AIStudio.Wpf.Controls;
using AIStudio.Wpf.DiagramDesigner.Demo.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : WindowBase
    {
        #region Identity
        private static IDictionary<string, Type> _viewDic;
        private static IDictionary<string, Type> _viewModelDic;

        static MainWindow()
        {
            _viewDic = new Dictionary<string, Type>();
            _viewModelDic = new Dictionary<string, Type>();
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.StartsWith("AIStudio.Wpf.DiagramDesigner.Demo"));
            assembly.GetTypes().Where(x => x?.Namespace?.StartsWith("AIStudio.Wpf.DiagramDesigner.Demo.Views") == true && x?.IsSubclassOf(typeof(UserControl))== true ).ToList().ForEach(x => _viewDic.Add(x.Name.Remove(x.Name.Length - 4), x));
            assembly.GetTypes().Where(x => x?.Namespace?.StartsWith("AIStudio.Wpf.DiagramDesigner.Demo.ViewModels") == true && x?.Name.Contains("ViewModel") == true).ToList().ForEach(x => _viewModelDic.Add(x.Name.Remove(x.Name.Length - 9), x));
        }
        #endregion
        List<MenuItemViewModel> _menus;
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;

            _menus = new List<MenuItemViewModel>()
            {
                new MenuItemViewModel(){Title = "Simple"},
                new MenuItemViewModel(){Title = "Locked"},
                new MenuItemViewModel(){Title = "Events"},
                new MenuItemViewModel(){Title = "DynamicInsertions"},
                new MenuItemViewModel(){Title = "Performance"},
                new MenuItemViewModel(){Title = "Zoom"},
                new MenuItemViewModel(){Title = "SnapToGrid"},
                new MenuItemViewModel(){Title = "DragAndDrop"},
                new MenuItemViewModel(){Title = "Nodes",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "Svg"},
                        new MenuItemViewModel(){Title = "CustomDefinedNode"},
                        new MenuItemViewModel(){Title = "PortlessLinks"},
                        new MenuItemViewModel(){Title = "GradientNode"},  
                        new MenuItemViewModel(){Title = "Rotate" },
                    }
                },
                new MenuItemViewModel(){Title = "Links",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "Snapping"},
                        new MenuItemViewModel(){Title = "Labels"},
                        new MenuItemViewModel(){Title = "Vertices"},
                        new MenuItemViewModel(){Title = "Markers"},
                        new MenuItemViewModel(){Title = "Routers"},
                        new MenuItemViewModel(){Title = "PathGenerators"},
                    }
                },
                new MenuItemViewModel(){Title = "Ports",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "ColoredPort"},
                        new MenuItemViewModel(){Title = "InnerPort"}
                    }
                },
                new MenuItemViewModel(){Title = "Groups",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "Group"},
                        new MenuItemViewModel(){Title = "CustomDefinedGroup"},
                        new MenuItemViewModel(){Title = "CustomShortcutGroup"},
                    }
                },
                 new MenuItemViewModel(){Title = "Texts",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "Text"},
                        new MenuItemViewModel(){Title = "Alignment"},
                        new MenuItemViewModel(){Title = "FontSize"},
                        new MenuItemViewModel(){Title = "ColorText"},
                        new MenuItemViewModel(){Title = "OutlineText"},
                    }
                },
                new MenuItemViewModel(){Title = "Customization",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "CustomNode"},
                        new MenuItemViewModel(){Title = "CustomLink"},
                        new MenuItemViewModel(){Title = "CustomPort"},
                        new MenuItemViewModel(){Title = "CustomGroup"},
                    }
                },
                new MenuItemViewModel(){Title = "Algorithms",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "ReconnectLinksToClosestPorts"},
                    }
                },
                new MenuItemViewModel(){Title = "Animations",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "PathAnimation"},
                        new MenuItemViewModel(){Title = "LineAnimation"},
                    }
                },
                new MenuItemViewModel(){Title = "Editor",
                    Children=new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel(){Title = "FlowchartEditor"},
                        new MenuItemViewModel(){Title = "MindEditor"},
                    }
                },
            };
            treeview.ItemsSource = _menus;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            treeview.SelectObject(_menus.FirstOrDefault());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var tag = button.Content?.ToString();

            ShowContent(tag);
        }

        private void ShowContent(string tag)
        {
            if (tag == null) return;

            if (_viewDic.ContainsKey(tag))
            {
                var control = Activator.CreateInstance(_viewDic[tag]) as UserControl;
                if (_viewModelDic.ContainsKey(tag))
                {
                    var viewmodel = Activator.CreateInstance(_viewModelDic[tag]);
                    if (viewmodel != null)
                    {
                        control.DataContext = viewmodel;
                    }
                }
                ContentControl.Content = control;
            }
            else
            {
                ContentControl.Content = new Controls.NotDoneYetControl();
            }
        }

        private void treeview_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!IsLoaded)
                return;

            if (e.NewValue is MenuItemViewModel menuItemViewModel && menuItemViewModel.Children == null)
            {
                ShowContent(menuItemViewModel.Title);
            }
        }
    }

    public class MenuItemViewModel
    {
        public string Title
        {
            get; set;
        }

        public List<MenuItemViewModel> Children
        {
            get; set;
        }
    }
}