﻿using System;
using AIStudio.Wpf.Mind.Helpers;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class MindEditorViewModel : BaseViewModel
    {
        public MindEditorViewModel()
        {
            Title = "MindEditor";
            Info = "Encapsulated mind controls";

            GetDataCommand = new SimpleCommand(GetDataExcute);
            SetDataCommand = new SimpleCommand(SetDataExcute);

             var theme = MindThemeHelper.SkyBlueTheme;
        }
      
        private Func<string> _getDataFunc;
        public Func<string> GetDataFunc
        {
            get
            {
                return _getDataFunc;
            }
            set
            {
                SetProperty(ref _getDataFunc, value);
            }
        }

        private string _inputData;
        public string InputData
        {
            get
            {
                return _inputData;
            }
            set
            {
                SetProperty(ref _inputData, value);
            }
        }

        private string _outputData;
        public string OutputData
        {
            get
            {
                return _outputData;
            }
            set
            {
                SetProperty(ref _outputData, value);
            }
        }

        private string _data = "{}";
        public string Data
        {
            get
            {
                return _data;
            }
            set
            {
                SetProperty(ref _data, value);
            }
        }


        public SimpleCommand GetDataCommand
        {
            get; private set;
        }

        public SimpleCommand SetDataCommand
        {
            get; private set;
        }

        private void GetDataExcute(object obj)
        {
            OutputData = GetDataFunc?.Invoke();
        }

        private void SetDataExcute(object obj)
        {
            Data = "{}";
            Data = InputData;
        }
    }
}
