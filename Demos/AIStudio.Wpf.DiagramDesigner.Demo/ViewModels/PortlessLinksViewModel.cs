﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class PortlessLinksViewModel : BaseViewModel
    {
        public PortlessLinksViewModel()
        {
            Title = "Portless Links";
            Info = "Starting from 2.0, you can create links between nodes directly! " +
                "All you need to specify is the shape of your nodes in order to calculate the connection points.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 80, Text = "1" };
            node1.ClearConnectors();
            DiagramViewModel.Add(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "2" };
            node2.ClearConnectors();
            DiagramViewModel.Add(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 200, Top = 300, Text = "3" };
            DiagramViewModel.Add(node3);

            DefaultDesignerItemViewModel node4 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 80, Top = 200, Text = "4" };
            node4.ClearConnectors();
            DiagramViewModel.Add(node4);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.PortlessConnector, node2.PortlessConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.PortlessConnector, node3.TopConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector2);

            ConnectionViewModel connector3 = new ConnectionViewModel(DiagramViewModel, node3.LeftConnector, node4.PortlessConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector3);
        }
    }
}