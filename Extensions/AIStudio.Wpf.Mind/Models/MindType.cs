﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.Mind.Models
{
    public enum MindType
    {
        [Description("思维导图")]
        Mind,
        [Description("目录组织图")]
        Directory,
        [Description("鱼骨头图")]
        FishBone,
        [Description("逻辑结构图")]
        Logical,
        [Description("组织结构图")]
        Organizational,
        [Description("天盘图(暂未完成)")]
        Celestial
    }
}
