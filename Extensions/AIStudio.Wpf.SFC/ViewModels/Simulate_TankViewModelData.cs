﻿using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class Simulate_TankViewModelData : BindableBase
    {
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                SetProperty(ref _title, value);
            }
        }

        public Simulate_TankViewModelData(LinkPoint linkPoint)
        {
            Title = "容器";
            LinkPoint = linkPoint;
        }

        /// <summary>
        /// 液位
        /// </summary>
        private LinkPoint linkPoint;
        public LinkPoint LinkPoint
        {
            get
            {
                return linkPoint;
            }
            set
            {
                SetProperty(ref linkPoint, value);
            }
        }       

    }
}
