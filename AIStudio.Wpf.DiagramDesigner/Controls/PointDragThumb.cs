﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner.Controls
{
    public class PointDragThumb : Thumb
    {
        public PointDragThumb()
        {
            base.DragDelta += new DragDeltaEventHandler(DragThumb_DragDelta);
            base.DragStarted += DragThumb_DragStarted;
            base.DragCompleted += DragThumb_DragCompleted;
        }


        private void DragThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (this.DataContext is ConnectorPointModel point)
            {
                point.DragStart = true;
            }
        }

        private void DragThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (this.DataContext is ConnectorPointModel point)
            {
                point.DragStart = false;
            }
        }

        void DragThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (this.DataContext is ConnectorPointModel point)
            {
                point.X += e.HorizontalChange;
                point.Y += e.VerticalChange;
            }
        }


    }
}
