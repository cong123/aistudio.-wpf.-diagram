﻿using AIStudio.Wpf.DiagramDesigner;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// 完整连接点
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(FullyCreatedConnectorInfoItem))]
    public class FullyCreatedConnectorInfoItem : ConnectorInfoItemBase
    {
        public FullyCreatedConnectorInfoItem()
        {

        }

        public FullyCreatedConnectorInfoItem(FullyCreatedConnectorInfo viewmodel) : base(viewmodel)
        {
            XRatio = viewmodel.XRatio;
            YRatio = viewmodel.YRatio;
            IsInnerPoint = viewmodel.IsInnerPoint;
            IsPortless = viewmodel.IsPortless;
        }


        [XmlAttribute]
        public double XRatio
        {
            get; set;
        }

        [XmlAttribute]
        public double YRatio
        {
            get; set;
        }

        [XmlAttribute]
        public bool IsInnerPoint
        {
            get; set;
        }

        [XmlAttribute]
        public bool IsPortless
        {
            get; set;
        }
    }
}
