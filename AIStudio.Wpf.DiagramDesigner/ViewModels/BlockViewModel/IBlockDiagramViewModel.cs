﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IBlockDiagramViewModel : IDiagramViewModel
    {
        ICommand AddNextCommand
        {
            get;
        }

        ICommand RemoveNextCommand
        {
            get;
        }

        ICommand InsertChildCommand
        {
            get;
        }

        ICommand RemoveChildCommand
        {
            get;
        }
    }
}
